
package ni.uni.Pojo;


public class Auto {
    
    private String Marca;
    private String Precio;
    private String Modelo;
    private String estado;

    public Auto() {
    }

    public Auto(String Marca, String Precio, String Modelo, String estado) {
        this.Marca = Marca;
        this.Precio = Precio;
        this.Modelo = Modelo;
        this.estado = estado;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getPrecio() {
        return Precio;
    }

    public void setPrecio(String Precio) {
        this.Precio = Precio;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }  
}
