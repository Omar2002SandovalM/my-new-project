
package DataReader;

import java.io.IOException;
import java.io.RandomAccessFile;
import ni.uni.Pojo.Auto;


public class Inventario {
    
      RandomAccessFile Inventario;
     int longuitud = 20;
     String salto = "\n";
    
     
    public void IngresarData(Auto F){
   
     try{
       Inventario = new RandomAccessFile("Inventario.txt", "rw");
       Inventario.seek(Inventario.length());
       
       int tamanoIngr = F.getMarca().length() + F.getPrecio().length() + F.getModelo().length();
       
       
       if(tamanoIngr > 20) { System.out.println("Error"); Inventario.close();}
       else{
       int aux = 20 - tamanoIngr;
       
       Inventario.writeBytes(F.getMarca()+",");
       Inventario.writeBytes(F.getPrecio()+",");
       Inventario.writeBytes(F.getModelo()+",");
       Inventario.writeBytes(F.getEstado()+",");
       for(int i = 0 ; i<aux - 4; i++){Inventario.writeBytes(" ");}
       Inventario.writeBytes(salto);
       
       Inventario.close();
       }
     }catch(IOException e){
        
     }
    
    }
    
     public void Registro(int registro)
    {
         try{
            Inventario = new RandomAccessFile("Inventario.txt", "r");
          
            Inventario.seek(registro * longuitud);
            
            String cade = Inventario.readLine();
            
            String datos [] ;

            datos = cade.split(",");
            
            if(datos[3].equals("*"))
                 System.out.println("No existe");
            else
             System.out.println(cade);
            
            
        }catch(IOException e){
            
        }
    }
    
    
    public void ImprimirData(){
        try{
            Inventario = new RandomAccessFile("Inventario.txt", "r");
            Inventario.seek(0);
            
            while(Inventario.getFilePointer() < Inventario.length())
            {
                System.out.println(Inventario.readLine());
            }
            
            Inventario.close();
        }catch(IOException e){
            
        }
    }
    
    //Pendiente Casi listo
    public boolean CambiarEstado(int registro, String marca, String mode, String precio)
    {
        try {
        Inventario = new RandomAccessFile("Inventario.txt", "rw");
          
        Inventario.seek(registro * longuitud);
            
        String cade = Inventario.readLine();
        
        String datos [] ;
        
        datos = cade.split(",");
        
        datos[0] = marca.trim(); 
        datos[1] = mode.trim();
        datos[2] = precio.trim();
        datos[3] = "*";
        
        Auto i = new Auto(datos[0],datos[1],datos[2],datos[3]);
        
        IngresarData(i);
        
        }catch(Exception e){}
        
        return true;
    }
    
    public boolean Eliminar(int registro)
    {
        try {
        Inventario = new RandomAccessFile("Inventario.txt", "rw");
          
        Inventario.seek(registro * longuitud);
            
        String cade = Inventario.readLine();
        
        String datos [];
        
        datos = cade.split(",");
        datos[3] = "*";
       
        /*------------------------------------------------*/
        Inventario.seek(registro * longuitud);
        
       Inventario.writeBytes(datos[0]+",");
       Inventario.writeBytes(datos[1]+",");
       Inventario.writeBytes(datos[2]+",");
       Inventario.writeBytes(datos[3]+",");
       Inventario.close();
        
       }catch(IOException e){}
        
        
        return true;
      }
}
