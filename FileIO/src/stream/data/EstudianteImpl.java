/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stream.data;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author CNEG-DOCENTE
 */
public class EstudianteImpl implements IDaoEstudiante {
    private DataOutputStream dos;
    private DataInputStream dis;
    private File file;
    private static final String FILENAME = "Estudiantes.dat";

    public EstudianteImpl() throws FileNotFoundException {
        load();
    }
    
    private void load() throws FileNotFoundException{
        file = new File(FILENAME);
        dos = new DataOutputStream(new FileOutputStream(file,true));
        dis = new DataInputStream(new FileInputStream(file));
    }
    @Override
    public Estudiante[] findByApellido(String apellido) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Estudiante[] findByNombre(String nombre) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Estudiante finbById(int id) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Estudiante findByCarnet(String carnet) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save(Estudiante t) throws IOException {
        if(t == null || dos == null){
            return;
        }
        dos.writeInt(t.getId());
        dos.writeUTF(t.getNombres());
        dos.writeUTF(t.getApellidos());
        dos.writeUTF(t.getCarnet());
        dos.writeInt(t.getEdad());        
    }

    @Override
    public void update(Estudiante t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Estudiante t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Estudiante[] addEstudiante(Estudiante e, Estudiante[] temp){
        if(temp == null){
            temp = new Estudiante[1];
            temp[temp.length - 1] = e;
            return temp;
        }
        
        temp = Arrays.copyOf(temp, temp.length + 1);
        temp[temp.length - 1] = e;
        return temp;
    }
    @Override
    public Estudiante[] findAll() throws IOException {
        Estudiante[] estudiantes = null;
        Estudiante e;
        while(dis.available() > 0){
            e = new Estudiante();
            e.setId(dis.readInt());
            e.setNombres(dis.readUTF());
            e.setApellidos(dis.readUTF());
            e.setCarnet(dis.readUTF());
            e.setEdad(dis.readInt());
            estudiantes = addEstudiante(e, estudiantes);
        }
        
        return estudiantes;
    }
    
    public void close() throws IOException{
        if(dos != null){
            dos.close();
        }
        if(dis != null){
            dis.close();
        }
    }
}
