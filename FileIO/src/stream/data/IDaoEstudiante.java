/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stream.data;

import java.io.IOException;

/**
 *
 * @author CNEG-DOCENTE
 */
public interface IDaoEstudiante extends IDao<Estudiante> {
    Estudiante[] findByApellido(String apellido)throws IOException;
    Estudiante[] findByNombre(String nombre)throws IOException;
    Estudiante finbById(int id)throws IOException;
    Estudiante findByCarnet(String carnet)throws IOException;
}
