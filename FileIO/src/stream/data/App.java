/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stream.data;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CNEG-DOCENTE
 */
public class App {
    public static void main(String[] args) {
        Estudiante pepito = new Estudiante(2, "Ana", "Conda", "2015-55251", 20);
        EstudianteImpl eimpl = null;
        
        try {
            eimpl = new EstudianteImpl();
//            eimpl.save(pepito);
            for(Estudiante e : eimpl.findAll()){
                System.out.println("Id: " + e.getId());
                System.out.println("Nombres: " + e.getNombres());
                System.out.println("Carnet: " + e.getCarnet());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                eimpl.close();
            } catch (IOException ex) {
                Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}
