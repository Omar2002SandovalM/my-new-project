/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stream.bytes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Estudiante
 */
public class WriteReadBytes {
    public static void main(String[] args) {
//        FileOutputStream fos;
//        FileInputStream fis;
//        String text = "";
//        int n = 0;
//        
        StreamIO sio = null;
        try {
//            fos = new FileOutputStream("hola.txt", true);
//            fis = new FileInputStream("hola.txt");
//            fos.write("hola buen dia a todos!".getBytes());
//            
//            while((n=fis.read())!= -1){
//                text += (char)n;
//            }
//            
//            System.out.println("Texto:");
//            System.out.println(text);

            sio = new StreamIO("hola.txt");
            //sio.writeString("Por la tarde tenemos clase");
            String text = sio.readString();
            System.out.println("Text:");
            System.out.println(text);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteReadBytes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteReadBytes.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                sio.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteReadBytes.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
                
    }
}
