/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stream.bytes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Estudiante
 */
public class StreamIO {
    private File file;
    private FileOutputStream fos;
    private FileInputStream fis;
    private String filename;

    public StreamIO(File file) throws FileNotFoundException {
        this.file = file;
        openStream();
    }

    public StreamIO(String filename) throws FileNotFoundException {
        this.filename = filename;
        file = new File(filename);
        openStream();
    }
    
    private void openStream() throws FileNotFoundException{
        fos = new FileOutputStream(file,true);
        fis = new FileInputStream(file);
    }
    
    public void writeString(String text) throws IOException{
        fos.write(text.getBytes());
    }
    
    public String readString() throws IOException{
        String text = "";
        int n = 0;
        
        while((n = fis.read()) != -1){
            text += (char)n;
        }
        return text;
    }
    
    public void close() throws IOException{
        if(fos != null){
            fos.close();
        }
        
        if(fis != null){
            fis.close();
        }
    }
}
